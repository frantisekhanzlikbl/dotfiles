set -g user_data ~/user_data
set -g app_root "$user_data/applications"

# some greeter to nicely match up with my prompt
set -g fish_greeting (set_color green)\
'┌─────────────────────────────────────────────────┐
│ Welcome to fish, the friendly interactive shell │
│ Type `help` for instructions on how to use fish │
├─────────────────────────────────────────────────┘'(set_color normal)
